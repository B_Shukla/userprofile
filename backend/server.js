const express = require('express');

const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const app = express();


connectionString = "mongodb+srv://admin:admin@cluster0.n1dzw.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
app.listen(3000, function () {
    console.log('listening on 3000')
});

MongoClient.connect(connectionString, { useUnifiedTopology: true })
    .then(client => {
        console.log('Connected to Database');
        const db = client.db('user');
        const users = db.collection('user');


        // Make sure you place body-parser before your CRUD handlers!
        app.use(express.urlencoded({ extended: true }))

        app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        app.get('/getuser', (req, res) => {
            db.collection('user').find().toArray()
                .then(results => {
                    console.log(results);
                    res.json(results);
                    return res;
                })
                .catch(error => console.error(error))
            // ...
        })

        app.post('/adduser', (req, res) => {
            users.insertOne(req.body)
                .then(result => {
                    console.log(result)
                })
                .catch(error => console.error(error))
        });

        app.delete('/deleteuser', (req, res) => {
            users.deleteOne(
              { id: req.body.id }
            )
              .then(result => {
                res.json(`Deleted successfully`)
              })
              .catch(error => console.error(error))
          })
    })
    .catch(error => console.error(error));



